<?php
/*
Plugin Name: mc Zitate
Plugin URI: http://myconceptions.de/plugins/
Description: Zeigt zufällig ein Zitat im Blog.
Version: 1.1.0
Author: Dominic Lautner
Author URI: http://www.myconceptions.de/
License: GNU
*/

/* plugin activation ************************************************************************************************ */

register_activation_hook( __FILE__, 'quotes_activation' );

function quotes_activation ()
{
    // rewrite flush 4 posttypes
    flush_rewrite_rules();
}

/* plugin deactivation ********************************************************************************************** */

register_deactivation_hook( __FILE__, 'quotes_deactivation' );

function quotes_deactivation ()
{
    // do something
}

/* add custom post type ********************************************************************************************* */

function create_cpt_quotes()
{
    $cpt_conf = array(
        'name'  => 'zitate',
        'slug'  => '',
        'tax'   => 'gruppe',
    );

    if ( $cpt_conf['name'] === '' )
    {
        $cpt_conf['slug'] = $cpt_conf['name'];
    }

    register_post_type( $cpt_conf['name'] ,
        array(
            'labels'      => array(
                'name'                => __('Zitate'),
                'singular_name'       => __('Zitat'),
                'add_new'             => __('Zitat erstellen'),
                'add_new_item'        => __('Neues Zitat'),
                'edit_item'           => __('Zitat bearbeiten'),
                'new_item'            => __('Neues Zitat'),
                'all_items'           => __('Alle Zitate'),
                'view_item'           => __('Zitate anzeigen'),
                'search_items'        => __('Zitat suchen'),
                'not_found'           => __('Kein passendes Zitat gefunden'),
                'not_found_in_trash'  => __('Kein Zitat im Papierkorb gefunden'),
                'parent_item_colon'   => '',
                'menu_name'           => __('Zitate')
            ),
            'public'              => true,
            'exclude_from_search' => true,
            'publicly_queryable'  => false,
            'show_ui'             => true,
            'show_in_nav_menus'   => false,
            'show_in_menu'        => true,
            'show_in_admin_bar'   => true,
            'menu_position'       => 5,
            'menu_icon'           => 'dashicons-format-quote',            // http://melchoyce.github.io/dashicons/
            'capability_type'     => 'page',
            'hierarchical'        => false,
            'supports'            => array(
                'title',
                'editor',
                //'author',
                //'thumbnail',
                //'excerpt',
                //'trackbacks',
                //'custom-fields',
                //'comments',
                //'revisions',
                //'page-attributes',
                //'post-formats'
            ),
            'register_meta_box_cb'=> null,
            'taxonomies'          => array(
                //'category',
                //'post_tag',
                'genre',
            ),
            'has_archive'         => false,
            'rewrite'             => array(
                'slug'  => $cpt_conf['slug'],
                'feeds' => false,
                'pages' => false
            ),
            'query_var'           => true,
        )

    );

}

add_action('init', 'create_cpt_quotes');

/* taxonomie ******************************************************************************************************** */

function create_quotes_taxonomies()
{
    $tax_conf = array(
        'cpt'   => 'zitate',
        'name'   => 'gruppe',
        'slug'   => '',
    );

    if ( $tax_conf['name'] === '' )
    {
        $tax_conf['slug'] = $tax_conf['name'];
    }

    register_taxonomy( $tax_conf['name'], $tax_conf['cpt'],
        array(
        'labels'            => array(
            'name'                          => _x( 'Gruppen', 'taxonomy general name' ), // tags & cats
            'singular_name'                 => _x( 'Gruppe', 'taxonomy singular name' ), // tags & cats
            'menu_name'                     => __( 'Gruppe' ), // tags & cats
            'all_items'                     => __( 'Alle Gruppen' ), // tags & cats
            'edit_item'                     => __( 'Bearbeite Gruppe' ), // tags & cats
            'view_item'                     => __( 'Zeige Gruppe' ), // tags & cats
            'update_item'                   => __( 'Update Gruppe' ), // tags & cats
            'add_new_item'                  => __( 'Gruppe hinzufügen' ), // tags & cats
            'new_item_name'                 => __( 'Neue Gruppe' ), // tags & cats
            'parent_item'                   => __( 'Parent Genre' ), // tags & cats
            'parent_item_colon'             => __( 'Parent Genre:' ), // tags & cats
            'search_items'                  => __( 'Suche Gruppe' ), // tags & cats
            'popular_itema'                 => __( 'Beliebte Gruppen' ), //tags
            'separate_items_with_commas'    => __( 'Gruppen mit Komma trennen' ), //tags
            'add_or_remove_items'           => __( 'Gruppe hinzufügen oder entfernen' ), //tags
            'choose_from_most_used'         => __( 'Meist genutzt Gruppen' ), //tags
            'not_found'                     => __( 'Keine Gruppe gefunden' ), //tags
            ),
            'public'            => true,
            'show_ui'           => true,
            'show_in_nav_menus' => true,
            'show_tagcloud'     => true,
            'show_admin_column' => true,
            'hierarchical'      => false, // true = category; false = tags
            'query_var'         => true,
            'rewrite'           => array(
                'slug'              => $tax_conf['slug']
            ),
            'sort'              => null,
            'capabilities'      => array(
                'manage_terms'      => 'manage_categories',
                'edit_terms'        => 'manage_categories',
                'delete_terms'      => 'manage_categories',
                'assign_terms'      => 'edit_posts',
            )
        )
    );
}

add_action( 'init', 'create_quotes_taxonomies' );

/* shortcodes ******************************************************************************************************* */

add_shortcode('zeige-zitat', 'show_rand_quote');

// [zeige-zitat]

function show_rand_quote($atts)
{
    $output = '';
    $orderby = '';
    $posts_per_page = '';

    extract(shortcode_atts(array(
        'posts_per_page' => 1,
        'orderby' => 'rand',
    ), $atts));

    $args = array(
        'post_type' => 'zitate',
        'post_status' => 'publish',
        'orderby' => $orderby,
        'posts_per_page' => $posts_per_page,
    );

    $_quotes = new WP_Query( $args );

    if ( $_quotes->have_posts() )
    {
        $output .= '<div id="quote-container">';

        while ( $_quotes->have_posts() )
        {
            $_quotes->the_post();

            $cf_zitat_von = get_post_meta ( get_the_ID(), '_cf_zitate_von', true );
            $cf_zitat_quelle = get_post_meta ( get_the_ID(), '_cf_zitate_quelle', true );
            $cf_zitat_link = get_post_meta ( get_the_ID(), '_cf_zitate_link', true );

            $output .= '<div class="content">';

            $output .= get_the_content();

            $output .= '</div>';

            if ( !empty( $cf_zitat_von ) )
            {
                $output .= '<div class="von">';

                $output .= '<span>' . $cf_zitat_von;

                if ( !empty( $cf_zitat_quelle ) )
                {
                    $output .= '<em class="quelle" title="Gefunden bei ' . $cf_zitat_quelle;

                    if ( !empty( $cf_zitat_link ) )
                    {
                        $output .= '"><a class="quote-link" href="' . $cf_zitat_link . '" traget="_blank">i</a></em>';
                    }
                    else
                    {
                        $output .= '">i</em>';
                    }

                }

                $output .= '</span>';

                $output .= '</div>';
            }

        }
        $output .= '</div>';
    }

    wp_reset_postdata();

    return $output;

}

/* meta box & custom fields ***************************************************************************************** */

add_action('add_meta_boxes', 'create_metaboxes_quotes');

function create_metaboxes_quotes()
{
    // add_meta_box(
    // $id,
    // $title,
    // $callback,
    // $post_type,
    // $context ('normal', 'advanced', or 'side'),
    // $priority ('high', 'core', 'default' or 'low'),
    // $callback_args );

    add_meta_box('mb_quotes', 'Zusatzinfos Zitate','add_metabox_content_quotes', 'zitate', 'advanced', 'default' );
}

function add_metabox_content_quotes($post)
{
    // Von
    $cf_zitate_von = get_post_meta ($post->ID, '_cf_zitate_von', true);

    ?>
    <div class="item f-left">
        <label for="cf_zitate_von"><?php echo __('Von:'); ?></label>
        <input type="text" name="cf_zitate_von" id="cf_zitate_von" value="<?php echo esc_attr( $cf_zitate_von );?>">
    </div>
    <?php

    // Quelle
    $cf_zitate_quelle = get_post_meta ($post->ID, '_cf_zitate_quelle', true);

    ?>
    <div class="item f-left">
        <label for="cf_zitate_quelle"><?php echo __('Quelle:'); ?></label>
        <input type="text" name="cf_zitate_quelle" id="cf_zitate_quelle" value="<?php echo esc_attr( $cf_zitate_quelle );?>">
    </div>
    <?php

    // Link
    $cf_zitate_link = get_post_meta ($post->ID, '_cf_zitate_link', true);

    ?>
    <div class="item f-left wide last">
        <label for="cf_zitate_link"><?php echo __('Link:'); ?> <span class="hint"><?php echo __('(mit http angeben)'); ?></span></label>
        <input type="text" name="cf_zitate_link" id="cf_zitate_link" value="<?php echo esc_attr( $cf_zitate_link );?>">
    </div>
    <?php

    echo '<br class="clear">';
}

add_action ('save_post', 'save_post_quotes');

function save_post_quotes( $post_id )
{
    if (isset($_POST['cf_zitate_von'])){
        update_post_meta($post_id, '_cf_zitate_von', strip_tags($_POST['cf_zitate_von']));
    }

    if (isset($_POST['cf_zitate_quelle'])){
        update_post_meta($post_id, '_cf_zitate_quelle', strip_tags($_POST['cf_zitate_quelle']));
    }

    if (isset($_POST['cf_zitate_link'])){
        update_post_meta($post_id, '_cf_zitate_link', strip_tags($_POST['cf_zitate_link']));
    }

}

/* admin css  ******************************************************************************************************* */

// fügt im admin das css an
add_action( 'admin_enqueue_scripts', 'css_quotes' );

// fügt auf der normalen seite das css an
add_action( 'wp_enqueue_scripts', 'css_quotes' );

// funktion die das css registriert und lädt
function css_quotes()
{
    wp_register_style( 'quotes-css', plugins_url( 'mc-quotes/quotes.css' ), false, '1.0.0' );
    wp_enqueue_style( 'quotes-css' );
}

/* imagessize ******************************************************************************************************* */

add_image_size( 'cpt-zitate-thumb', '80', '80', false );
add_image_size( 'cpt-zitate-image', '120', '120', false );

/* widget *********************************************************************************************************** */


add_action( 'widgets_init', 'init_quotes_widget' );

function init_quotes_widget()
{
    register_widget( 'quotes_widget' );
}
class quotes_widget extends WP_Widget {

    function quotes_widget()
    {
        $options = array(
            'classname' => 'quotes_widget',
            'description' => __('Zeigt ein zufälliges Zitat in der Sidebar'),
        );

        $this->WP_Widget('quotes_widget', __('Zitate Widget'), $options);
    }

    function widget( $args, $instance )
    {
        extract($args);

        $title = $instance['title'];

        echo $before_widget;

        if ( $title )
        {
            echo '<h3 class="title">' . $title . '</h3>';
        }

        ?>
        <div class="quotes-widget">
            <?php echo do_shortcode('[zeige-zitat]'); ?>
        </div>
        <?php

        echo $after_widget;
    }

    function form( $instance )
    {
        $instance = wp_parse_args( (array)$instance );

        ?>
<p>
<label for="<?php echo $this->get_field_id( 'title' ); ?>"><?php _e('Titel:'); ?></label>
<input id="<?php echo $this->get_field_id( 'title' ); ?>" class="widefat" type="text" value="<?php echo $instance['title']; ?>" name="<?php echo $this->get_field_name( 'title' ); ?>" >
</p>
<?php

    }

    function update( $new_instance, $old_instance )
    {
        $instance = $old_instance;

        //Strip tags from title and name to remove HTML
        $instance['title'] = strip_tags( $new_instance['title']);

        return $instance;
    }
}

/*eof*/
?>